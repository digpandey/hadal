import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { SettingsComponent } from './components/settings/settings.component';
import { LoginpageComponent } from './components/loginpage/loginpage.component';
import { RegisterComponent } from './components/register/register.component';
import { MessageComponent } from './components/message/message.component';
import { FooterComponent } from './components/footer/footer.component';
import { AddimgComponent } from './components/body/addimg/addimg.component';
import { UsernameComponent } from './components/body/username/username.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './components/settings/profile/profile.component';
import { ImguploadComponent } from './components/imgupload/imgupload.component';
import { PhotosComponent } from './components/photos/photos.component';
import { VideosComponent } from './components/videos/videos.component';
import { TagsComponent } from './components/tags/tags.component';
import { HttpClientModule } from '@angular/common/http'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    NotificationsComponent,
    SettingsComponent,
    LoginpageComponent,
    RegisterComponent,
    MessageComponent,
    FooterComponent,
    AddimgComponent,
    UsernameComponent,
    ProfileComponent,
    ImguploadComponent,
    PhotosComponent,
    VideosComponent,
    TagsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
