import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginpageComponent } from './components/loginpage/loginpage.component';
import { MessageComponent } from './components/message/message.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { PhotosComponent } from './components/photos/photos.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/settings/profile/profile.component';
import { SettingsComponent } from './components/settings/settings.component';
import { TagsComponent } from './components/tags/tags.component';
import { VideosComponent } from './components/videos/videos.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'home',component:HomeComponent},
  {path:'message',component:MessageComponent},
  {path:'notification',component:NotificationsComponent},
  {path:'setting',component:SettingsComponent},
  {path:'loginpage',component:LoginpageComponent},
  {path:'register',component:RegisterComponent},
  {path:'profile',component:ProfileComponent},
  {path:'photos',component:PhotosComponent},
  {path:'videos',component:VideosComponent},
  {path:'tags',component:TagsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
