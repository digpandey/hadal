import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-addimg',
  templateUrl: './addimg.component.html',
  styleUrls: ['./addimg.component.css']
})
export class AddimgComponent implements OnInit {
  imageURL: string;
  uploadForm: FormGroup;
  name = "Kabir Pandey"
  counter = 0;

  constructor(public fb: FormBuilder) {

    // Reactive Form
    this.uploadForm = this.fb.group({
      avatar: [null],
      name: ['']
    })
  }

  ngOnInit(): void {

  }
  // Image Preview
  showPreview(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.uploadForm.patchValue({
      avatar: file
    });
    this.uploadForm.get('avatar').updateValueAndValidity()
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
    }
    reader.readAsDataURL(file)
    console.log(file)//this is here 
  }

  
  // Submit Form
  submit() {
    console.log(this.uploadForm.value)
  }
  likes() {
    this.counter++;
  }
  comments(){
    alert("this is comment");
  }
}
