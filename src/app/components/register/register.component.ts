import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  myForm: FormGroup;
  popForm: FormGroup;
  http: any;
  contData: any = {};
  constructor(private fb: FormBuilder, http: HttpClient) {
    this.myForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.pattern("[a-zA-Z 0-9 /.]+@+")]],
        password: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9]{10}")]],
        text: ['', [Validators.required, Validators.pattern]]
      })
  }
  ngOnInit(): void {
  }

  //adding in local storage
  get() {
    confirm("are you sure to submit");
  }
  xyz() {
    console.log(this.myForm.value);
    this.contData = Object.assign(this.contData, this.myForm.value);
    this.addUser(this.contData)
  }
  //for adding multiple value
  addUser(contData) {
    let newAdd = [this.contData];
    if (localStorage.getItem('Data')) {
      newAdd = JSON.parse(localStorage.getItem('Data'))
      newAdd = [...newAdd,contData ];
    }
    localStorage.setItem("Data", JSON.stringify(newAdd));
  }
  //here we start popups
  abc() {
    // alert("hii");
    document.getElementById("popupForm").style.display = "block";
  }
  wer() {
    document.getElementById("popupForm").style.display = "none";
  }
  fgh() {
    let popdata = this.popForm.getRawValue();
    console.log(popdata);
  }
}
